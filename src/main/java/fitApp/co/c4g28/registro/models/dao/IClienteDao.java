package fitApp.co.c4g28.registro.models.dao;

import org.springframework.data.repository.CrudRepository;

import fitApp.co.c4g28.registro.models.entities.Cliente;

public interface IClienteDao extends CrudRepository <Cliente, Long> {

}
