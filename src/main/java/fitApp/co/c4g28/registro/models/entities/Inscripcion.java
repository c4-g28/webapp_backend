package fitApp.co.c4g28.registro.models.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="inscripciones")
public class Inscripcion implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="codigoInscripcion")
	private Long codigoInscripcion;
	
	@Column(name="fechaInscripcion")
	private LocalDate fechaInscripcion;
	
	@Column(name="fechaFinInscripcion")
	private LocalDate fechaFinInscripcion;
	
	/* @ManyToOne//(cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
	@JoinColumn(name="idCliente")
	private Cliente idCliente; */
	
	@ManyToOne
	@JoinColumn(name="codigoMes")
	private Mensual codigoMes;
	
	@ManyToOne
	@JoinColumn(name="codigoTrimestral")
	private Trimestral codigoTrimestral;
	
	@ManyToOne
	@JoinColumn(name="codigoAnio")
	private Anual codigoAnio;

	
	public Long getCodigoInscripcion() {
		return codigoInscripcion;
	}

	public void setCodigoInscripcion(Long codigoInscripcion) {
		this.codigoInscripcion = codigoInscripcion;
	}

	public LocalDate getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(LocalDate fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

	public LocalDate getFechaFinInscripcion() {
		return fechaFinInscripcion;
	}

	public void setFechaFinInscripcion(LocalDate fechaFinInscripcion) {
		this.fechaFinInscripcion = fechaFinInscripcion;
	}

	/* public Cliente getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Cliente idCliente) {
		this.idCliente = idCliente;
	} */
			
}