package fitApp.co.c4g28.registro.models.services;

import fitApp.co.c4g28.registro.models.entities.Usuario;

public interface IUsuarioService {

	public Usuario findByUsername(String username);
}
