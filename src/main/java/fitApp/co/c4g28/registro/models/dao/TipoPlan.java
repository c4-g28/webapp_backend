package fitApp.co.c4g28.registro.models.dao;

public interface TipoPlan {

	Double getPrecio();
	Double setPrecioClase();
}
