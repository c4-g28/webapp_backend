package fitApp.co.c4g28.registro.models.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import fitApp.co.c4g28.registro.models.dao.TipoPlan;

@Entity
@Table(name = "anual")
public class Anual implements TipoPlan, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idMesPago")
	private Double idMesPago;

	@Column(name = "codPlan")
	private Double codPlan;

	@Column(name = "precioPlan")
	private Double precioPlan;

	@Override
	public Double getPrecio() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double setPrecioClase() {
		// TODO Auto-generated method stub
		return null;
	}

}
