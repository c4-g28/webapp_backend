package fitApp.co.c4g28.registro.models.services;

import java.util.List;

import fitApp.co.c4g28.registro.models.entities.Cliente;

public interface IClienteService {

	public List<Cliente> findAll();
	
	public Cliente findById(Long id);
	
	public Cliente save(Cliente cliente);
	
	public void delete(Cliente cliente);
}
