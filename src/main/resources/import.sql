INSERT INTO anual (id_mes_pago, cod_plan, precio_plan) VALUES ('1', '1', '800000');
INSERT INTO mensual (id_mes_pago, cod_plan, precio_plan) VALUES ('1', '1', '60000');
INSERT INTO trimestral (id_mes_pago, cod_plan, precio_plan) VALUES ('1', '1', '150000');

INSERT INTO inscripciones (codigo_inscripcion, fecha_inscripcion, fecha_fin_inscripcion, codigo_mes, codigo_trimestral, codigo_anio) VALUES ('1', '2021-10-20', '2022-10-20', '1', '1', '1');

INSERT INTO clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Andrés', 'Guzmán', 'profesor@bolsadeideas.com', '2018-01-01', 1);
INSERT INTO clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Mr. John', 'Doe', 'john.doe@gmail.com', '2018-01-02', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Diego', 'Fernando', 'diego@gmail.com', '2018-01-01', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Eduardo', 'Valdivieso', 'eduardo@gmail.com', '2018-01-02', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Angelica', 'Ramos', 'angelica.ramos@gmail.com', '2018-01-03', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Juan', 'Arias', 'juan.arias@gmail.com', '2018-01-04', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Dory', 'Mesa', 'erich.gamma@gmail.com', '2018-02-01', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Richard', 'Helm', 'richard.helm@gmail.com', '2018-02-10', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Ralph', 'Johnson', 'ralph.johnson@gmail.com', '2018-02-18', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('John', 'Vlissides', 'john.vlissides@gmail.com', '2018-02-28', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Dr. James', 'Gosling', 'james.gosling@gmail.com', '2018-03-03', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Magma', 'Lee', 'magma.lee@gmail.com', '2018-03-04', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Tornado', 'Roe', 'tornado.roe@gmail.com', '2018-03-05', 1);
INSERT INTO Clientes (nombre, apellido, email, fecha, codigo_inscripcion) VALUES('Jade', 'Doe', 'jane.doe@gmail.com', '2018-03-06', 1);

INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');


INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('andres','$2a$10$pLQvxwsrJWNMmPxyaFVPGujMkcS7shC0SJqoLXVzFVFSveid3eiKy',1, 'Andres', 'Guzman','profesor@gmail.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin','$2a$10$xL1HiyRWfJrPZ.86dhjxO.OR7GerM/0Bh9BssLqvuxbTZZOkryjQ2',1, 'John', 'Doe','jhon.doe@gmail.com');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);